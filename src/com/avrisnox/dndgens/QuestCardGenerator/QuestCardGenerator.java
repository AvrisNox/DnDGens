package com.avrisnox.dndgens.QuestCardGenerator;

import com.avrisnox.dndgens.QuestCardGenerator.Objectives.Objective;
import com.avrisnox.dndgens.QuestCardGenerator.Objectives.ObjectiveModifier;
import com.avrisnox.dndgens.QuestCardGenerator.Objectives.ObjectiveType;
import com.avrisnox.dndgens.QuestCardGenerator.Rewards.Reward;
import com.avrisnox.dndgens.QuestCardGenerator.Rewards.RewardModifier;
import com.avrisnox.dndgens.QuestCardGenerator.Rewards.RewardType;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;


public class QuestCardGenerator {
    private Map<String, List<String>> replacements;
    private List<ObjectiveModifier> objectiveMods;
    private List<ObjectiveType> objectiveTypes;
    private List<RewardModifier> rewardMods;
    private List<RewardType> rewardTypes;
    private List<Fixer> fixers;

    public float OBJECTIVE_COMPLEX_OVER_SIMPLE = 0.4f;
    public float REWARD_VALUABLE_OVER_REWARD = 0.1f;

    public QuestCardGenerator(Map<String, List<String>> replacements, List<ObjectiveModifier> objectiveMods, List<ObjectiveType> objectiveTypes, List<RewardModifier> rewardMods, List<RewardType> rewardTypes, List<Fixer> fixers) {
        this.replacements = replacements;
        this.objectiveMods = objectiveMods;
        this.objectiveTypes = objectiveTypes;
        this.rewardMods = rewardMods;
        this.rewardTypes = rewardTypes;
        this.fixers = fixers;
        verify();
    }

    public Map<String, List<String>> getReplacements() {
        return replacements;
    }

    public void setReplacements(Map<String, List<String>> replacements) {
        this.replacements = replacements;
        verify();
    }

    public List<ObjectiveModifier> getObjectiveMods() {
        return objectiveMods;
    }

    public void setObjectiveMods(List<ObjectiveModifier> objectiveMods) {
        this.objectiveMods = objectiveMods;
        verify();
    }

    public List<ObjectiveType> getObjectiveTypes() {
        return objectiveTypes;
    }

    public void setObjectiveTypes(List<ObjectiveType> objectiveTypes) {
        this.objectiveTypes = objectiveTypes;
        verify();
    }

    public List<RewardModifier> getRewardMods() {
        return rewardMods;
    }

    public void setRewardMods(List<RewardModifier> rewardMods) {
        this.rewardMods = rewardMods;
        verify();
    }

    public List<RewardType> getRewardTypes() {
        return rewardTypes;
    }

    public void setRewardTypes(List<RewardType> rewardTypes) {
        this.rewardTypes = rewardTypes;
        verify();
    }

    public List<Fixer> getFixers() {
        return fixers;
    }

    public void setFixers(List<Fixer> fixers) {
        this.fixers = fixers;
        verify();
    }

    private void verify() {
        if(replacements == null)
            throw new IllegalArgumentException("Replacement map is null.");
        if(objectiveMods == null)
            throw new IllegalArgumentException("Objective modifiers list is null.");
        if(objectiveTypes == null)
            throw new IllegalArgumentException("Objective type list is null.");
        if(rewardMods == null)
            throw new IllegalArgumentException("Reward modifiers list is null.");
        if(rewardTypes == null)
            throw new IllegalArgumentException("Reward types list is null.");
        if(fixers == null)
            throw new IllegalArgumentException("Fixers list is null.");
    }

    private boolean inTolerance(float maxC, float currC, boolean challenge) {
        float maxTol = maxC + maxC * (challenge ? Settings.OBJECTIVE_CHALLENGE_TOLERANCE : Settings.REWARD_COST_TOLERANCE);
        float minTol = maxC - maxC * (challenge ? Settings.OBJECTIVE_CHALLENGE_TOLERANCE : Settings.REWARD_COST_TOLERANCE);
        return minTol <= currC && currC <= maxTol;
    }

    /**
     * Generates a single random objective.
     * @param challenge The challenge rating for this objective. Determines the overall difficulty of the objective and its children.
     * @param volatility The volatility of randomness for this objective. High volatility may result in more children and modifiers (both good and bad).
     * @return An objective.
     */
    public Objective generateRandomObjective(float challenge, float volatility) {
        verify();
        // TODO
        return null;
    }

    /**
     * Generates a single random reward.
     * @param cost The cost rating for this reward. Determines the overall value of what's returned.
     * @param volatility The volatility of randomness for this reward. High volatility may result in a lower base-value item with good mods or a higher base-value item with bad mods.
     * @return A reward.
     */
    public Reward generateRandomReward(float cost, float volatility) {
        verify();
        // TODO
        return null;
    }

    /**
     * A bare-bones random quest card generator.
     * @param totalChallenge [0,inf) The overall challenge of the collective tasks. A basic challenge is party count * level / 4.
     * @param minChallenge [0,totalChallenge] The minimum challenge for individual objectives. When minChallenge = totalChallenge, this ensures a single objective.
     * @param maxChallenge (0,inf) The maximum challenge for individual objectives. When maxChallenge >= totalChallenge, there might be a single objective.
     * @param volatility [0,1] The volatility of individual challenges and rewards. Increase this number to see more modifiers and child objectives.
     * @return A quest card - complete with Fixer, Objectives, and Rewards - which can then be tweaked as needed (I.E., for broker and time).
     */
    public QuestCard generateRandomQuestCard(float totalChallenge, float minChallenge, float maxChallenge, float volatility) {
        return this.generateRandomQuestCard(Settings.CHALLENGE_MODIFIER, QuestCard.getLevelFromCP(totalChallenge, Settings.CHALLENGE_MODIFIER), totalChallenge, minChallenge, maxChallenge, volatility);
    }

    /**
     * A somewhat involved random quest card generator.
     * @param party [1,inf) The number of players in the party. If NPCs or magical items are assisting, bring this number up slightly.
     * @param level [0,inf) The level of players in the party. If NPCs or magical items are assisting, bring this number up slightly.
     * @param totalChallenge [0,inf) The overall challenge of the collective tasks. A basic challenge is party count * level / 4.
     * @param minChallenge [0,totalChallenge] The minimum challenge for individual objectives. When minChallenge = totalChallenge, this ensures a single objective.
     * @param maxChallenge (0,inf) The maximum challenge for individual objectives. When maxChallenge >= totalChallenge, there might be a single objective.
     * @param volatility [0,1] The volatility of individual challenges and rewards. Increase this number to see more modifiers and child objectives.
     * @return A quest card - complete with Fixer, Objectives, and Rewards - which can then be tweaked as needed (I.E., for broker and time).
     */
    public QuestCard generateRandomQuestCard(float party, float level, float totalChallenge, float minChallenge, float maxChallenge, float volatility) {
        Random r = new Random();
        float diff = maxChallenge - minChallenge;

        // Generate Objective list
        List<Objective> objectives = new LinkedList<>();
        float runningCR = 0.0f;
        while(!inTolerance(maxChallenge, runningCR, true) && runningCR < maxChallenge) {
            float ccr, cost;
            float remainingCost = totalChallenge - runningCR;
            float remainingCR = objectives.size() > 0 ? remainingCost / Settings.MULTI_OBJECTIVE_CHALLENGE_MOD : remainingCost;
            if(remainingCost > maxChallenge) {
                ccr = minChallenge + (r.nextFloat() % diff);
                cost = objectives.size() > 0 ? ccr * Settings.MULTI_OBJECTIVE_CHALLENGE_MOD : ccr;
            } else if(remainingCost <= maxChallenge && remainingCost > minChallenge) {
                ccr = remainingCR;
                cost = remainingCost;
            } else if(!Settings.DROP_EXCESS_OBJECTIVE_COST) {
                ccr = minChallenge;
                cost = objectives.size() > 0 ? ccr * Settings.MULTI_OBJECTIVE_CHALLENGE_MOD : ccr;
            } else
                break;

            objectives.add(generateRandomObjective(ccr, volatility));
            runningCR += cost;
        }

        // Generate Reward list
        List<Reward> rewards = new LinkedList<>();
        float runningCost = 0.0f;
        while(!inTolerance(maxChallenge, runningCost, false) && runningCost < maxChallenge) {
            float cost;
            float remainingCost = totalChallenge - runningCost;

            if(remainingCost > maxChallenge) cost = minChallenge + (r.nextFloat() % diff);
            else if(remainingCost <= maxChallenge && remainingCost > minChallenge) cost = remainingCost;
            else if(!Settings.DROP_EXCESS_REWARD_COST) cost = minChallenge;
            else break;

            rewards.add(generateRandomReward(cost, volatility));
            runningCost += cost;
        }

        return new QuestCard(
                "A random quest",
                "A randomized quest generated by the Quest Card Generator!",
                "The QuestCardGenerator system",
                totalChallenge,
                party,
                level,
                QuestCard.getSuggestedFixerFromObjectives(objectives, fixers),
                objectives,
                rewards
        );
    }
}

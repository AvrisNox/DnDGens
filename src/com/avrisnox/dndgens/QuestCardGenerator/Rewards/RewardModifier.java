package com.avrisnox.dndgens.QuestCardGenerator.Rewards;

import com.avrisnox.dndgens.QuestCardGenerator.Objectives.ObjectiveType;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class RewardModifier {
    String name;
    String description;
    float costModifier;
    Image icon;
    java.util.List<ObjectiveType> possibleObjTypes;

    public RewardModifier() {
        this(
                "Empty modifier",
                "An empty reward modifier",
                1.0f,
                (Image)null,
                null
        );
    }

    public RewardModifier(String name, String description, float costModifier, Image icon, java.util.List<ObjectiveType> possibleObjTypes) {
        this.name = name;
        this.description = description;
        this.costModifier = costModifier;
        this.icon = icon;
        this.possibleObjTypes = possibleObjTypes;

    }

    public RewardModifier(String name, String description, float costModifier, String iconPath, java.util.List<ObjectiveType> possibleObjTypes) {
        this(
                name,
                description,
                costModifier,
                (Image)null,
                possibleObjTypes
        );
        setIcon(iconPath);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getCostModifier() {
        return costModifier;
    }

    public void setCostModifier(float costModifier) {
        this.costModifier = costModifier;
    }

    public Image getIcon() {
        return icon;
    }

    public void setIcon(Image icon) {
        this.icon = icon;
    }

    public boolean setIcon(String path) {
        boolean result = false;
        try {
            icon = ImageIO.read(new File(path));
            result = true;
        } catch(IOException ignored) {
            System.out.println("Failed to set Reward Modifier " + this.getName() + " icon with path " + path);
        }
        return result;
    }

    public java.util.List<ObjectiveType> getPossibleObjTypes() {
        return possibleObjTypes;
    }

    public void setPossibleObjTypes(List<ObjectiveType> possibleObjTypes) {
        this.possibleObjTypes = possibleObjTypes;
    }

    public void addPossibleObjTypes(ObjectiveType possibleObjType) {
        if(possibleObjTypes == null)
            possibleObjTypes = new LinkedList<>();
        possibleObjTypes.add(possibleObjType);
    }

    public boolean remPossibleObjTypes(ObjectiveType possibleObjType) {
        if(possibleObjTypes == null)
            return false;
        return possibleObjTypes.remove(possibleObjType);
    }

    public void clearPossibleObjTypes() {
        possibleObjTypes = new LinkedList<>();
    }
}

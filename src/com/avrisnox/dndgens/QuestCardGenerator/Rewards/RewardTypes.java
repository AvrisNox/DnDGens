package com.avrisnox.dndgens.QuestCardGenerator.Rewards;

import java.util.Arrays;
import java.util.List;

public enum RewardTypes implements RewardType{
    ARMOR("Some pretty average armor.", ARMOR_TYPES.LIGHT, ARMOR_TYPES.MEDIUM, ARMOR_TYPES.HEAVY),
    WEAPON("A pretty average weapon.", WEAPON_TYPES.SIMPLE_MELEE, WEAPON_TYPES.SIMPLE_RANGE, WEAPON_TYPES.MARTIAL_MELEE, WEAPON_TYPES.MARTIAL_RANGE),
    SHIELD("A pretty average shield."),
    SCROLL("A strange scroll of{spell}."),
    POTION("A strange potion of{potion}."),
    VALUABLE("A valuable item worth around{value}{currency}.", VALUABLE_TYPES.ART, VALUABLE_TYPES.GEMS, VALUABLE_TYPES.COINS, VALUABLE_TYPES.SPECIAL, VALUABLE_TYPES.ARTIFACTS),
    OTHER("A strange item.{qualifier}"),
    UNIQUE("{desc}");

    final String description;
    final List<RewardType> children;
    RewardTypes(String description, RewardType... children) {
        this.description = description;
        this.children = Arrays.asList(children);
    }

    @Override
    public String getName() {
        return this.name();
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public RewardType getParent() {
        return null;
    }

    @Override
    public List<RewardType> getChildren() {
        return children;
    }

    public enum ARMOR_TYPES implements RewardType {
        LIGHT("Some average light armor.", LIGHT_TYPES.PADDED, LIGHT_TYPES.LEATHER, LIGHT_TYPES.STUDDED_LEATHER, LIGHT_TYPES.SPECIAL),
        MEDIUM("Some average medium armor.", MEDIUM_TYPES.HIDE, MEDIUM_TYPES.CHAINSHIRT, MEDIUM_TYPES.SCALEMAIL, MEDIUM_TYPES.BREASTPLATE, MEDIUM_TYPES.HALFPLATE, MEDIUM_TYPES.SPIKED, MEDIUM_TYPES.SPECIAL),
        HEAVY("Some average heavy armor.", HEAVY_TYPES.RINGMAIL, HEAVY_TYPES.CHAINMAIL, HEAVY_TYPES.SPLINT, HEAVY_TYPES.PLATE, HEAVY_TYPES.SPECIAL),
        SPECIAL("Some special armor.{qualifier}");

        final String description;
        final List<RewardType> children;
        ARMOR_TYPES(String description, RewardType... children) {
            this.description = description;
            this.children = Arrays.asList(children);
        }

        @Override
        public String getName() {
            return this.name();
        }

        @Override
        public String getDescription() {
            return description;
        }

        @Override
        public RewardType getParent() {
            return ARMOR;
        }

        @Override
        public List<RewardType> getChildren() {
            return children;
        }

        public enum LIGHT_TYPES implements RewardType {
            PADDED("Some average padded armor."),
            LEATHER("Some average leather armor."),
            STUDDED_LEATHER("Some average studded leather armor."),
            SPECIAL("Some special light armor.{qualifier}");

            final String description;
            final List<RewardType> children;
            LIGHT_TYPES(String description, RewardType... children) {
                this.description = description;
                this.children = Arrays.asList(children);
            }

            @Override
            public String getName() {
                return this.name();
            }

            @Override
            public String getDescription() {
                return description;
            }

            @Override
            public RewardType getParent() {
                return LIGHT;
            }

            @Override
            public List<RewardType> getChildren() {
                return children;
            }
        }

        public enum MEDIUM_TYPES implements RewardType {
            HIDE("Some average hide armor."),
            CHAINSHIRT("An average chainmail shirt."),
            SCALEMAIL("Some average scalemail armor."),
            SPIKED("Some average spiked armor."),
            BREASTPLATE("Some average breastplate armor."),
            HALFPLATE("Some average halfplate armor."),
            SPECIAL("Some special medium armor.{qualifier}");

            final String description;
            final List<RewardType> children;
            MEDIUM_TYPES(String description, RewardType... children) {
                this.description = description;
                this.children = Arrays.asList(children);
            }

            @Override
            public String getName() {
                return this.name();
            }

            @Override
            public String getDescription() {
                return description;
            }

            @Override
            public RewardType getParent() {
                return MEDIUM;
            }

            @Override
            public List<RewardType> getChildren() {
                return children;
            }
        }

        public enum HEAVY_TYPES implements RewardType {
            RINGMAIL("Some average ringmail armor."),
            CHAINMAIL("Some average chainmail armor."),
            SPLINT("Some average splint armor."),
            PLATE("Some average plate armor."),
            SPECIAL("Some special heavy armor.{qualifier}");

            final String description;
            final List<RewardType> children;
            HEAVY_TYPES(String description, RewardType... children) {
                this.description = description;
                this.children = Arrays.asList(children);
            }

            @Override
            public String getName() {
                return this.name();
            }

            @Override
            public String getDescription() {
                return description;
            }

            @Override
            public RewardType getParent() {
                return HEAVY;
            }

            @Override
            public List<RewardType> getChildren() {
                return children;
            }
        }
    }

    public enum WEAPON_TYPES implements RewardType {
        SIMPLE_MELEE("An average simple melee weapon.", SIMPLE_MELEE_TYPES.CLUB, SIMPLE_MELEE_TYPES.DAGGER, SIMPLE_MELEE_TYPES.GREATCLUB, SIMPLE_MELEE_TYPES.HANDAXE, SIMPLE_MELEE_TYPES.JAVELIN, SIMPLE_MELEE_TYPES.LIGHTHAMMER, SIMPLE_MELEE_TYPES.MACE, SIMPLE_MELEE_TYPES.QUARTERSTAFF, SIMPLE_MELEE_TYPES.SICKLE, SIMPLE_MELEE_TYPES.SPEAR, SIMPLE_MELEE_TYPES.SPECIAL),
        SIMPLE_RANGE("An average simple ranged weapon.", SIMPLE_RANGE_TYPES.DART, SIMPLE_RANGE_TYPES.SLING, SIMPLE_RANGE_TYPES.SHORTBOW, SIMPLE_RANGE_TYPES.LIGHTCROSSBOW, SIMPLE_RANGE_TYPES.SPECIAL),
        MARTIAL_MELEE("An average martial melee weapon.", MARTIAL_MELEE_TYPES.BATTLEAXE, MARTIAL_MELEE_TYPES.FLAIL, MARTIAL_MELEE_TYPES.MAUL, MARTIAL_MELEE_TYPES.GLAIVE, MARTIAL_MELEE_TYPES.GREATAXE, MARTIAL_MELEE_TYPES.GREATSWORD, MARTIAL_MELEE_TYPES.HALBERD, MARTIAL_MELEE_TYPES.LANCE, MARTIAL_MELEE_TYPES.LONGSWORD, MARTIAL_MELEE_TYPES.MORNINGSTAR, MARTIAL_MELEE_TYPES.PIKE, MARTIAL_MELEE_TYPES.RAPIER, MARTIAL_MELEE_TYPES.SCIMITAR, MARTIAL_MELEE_TYPES.SHORTSWORD, MARTIAL_MELEE_TYPES.SPECIAL, MARTIAL_MELEE_TYPES.TRIDENT, MARTIAL_MELEE_TYPES.TRIDENT, MARTIAL_MELEE_TYPES.WARHAMMER, MARTIAL_MELEE_TYPES.WARPICK, MARTIAL_MELEE_TYPES.WHIP),
        MARTIAL_RANGE("An average martial ranged weapon.", MARTIAL_RANGE_TYPES.BLOWGUN, MARTIAL_RANGE_TYPES.HANDCROSSBOW, MARTIAL_RANGE_TYPES.SPECIAL, MARTIAL_RANGE_TYPES.HEAVYCROSSBOW, MARTIAL_RANGE_TYPES.LONGBOW, MARTIAL_RANGE_TYPES.NET),
        SPECIAL("A special weapon.{qualifier}");

        final String description;
        final List<RewardType> children;
        WEAPON_TYPES(String description, RewardType... children) {
            this.description = description;
            this.children = Arrays.asList(children);
        }

        @Override
        public String getName() {
            return this.name();
        }

        @Override
        public String getDescription() {
            return description;
        }

        @Override
        public RewardType getParent() {
            return WEAPON;
        }

        @Override
        public List<RewardType> getChildren() {
            return children;
        }

        public enum SIMPLE_MELEE_TYPES implements RewardType {
            CLUB("An average club."),
            DAGGER("An average dagger."),
            GREATCLUB("An average great-club."),
            HANDAXE("An average hand-axe."),
            JAVELIN("An average javelin."),
            LIGHTHAMMER("An average light hammer."),
            MACE("An average mace."),
            QUARTERSTAFF("An average quarterstaff."),
            SICKLE("An average sickle."),
            SPEAR("An average spear."),
            SPECIAL("Some special simple melee weapon.{qualifier}");

            final String description;
            final List<RewardType> children;
            SIMPLE_MELEE_TYPES(String description, RewardType... children) {
                this.description = description;
                this.children = Arrays.asList(children);
            }

            @Override
            public String getName() {
                return this.name();
            }

            @Override
            public String getDescription() {
                return description;
            }

            @Override
            public RewardType getParent() {
                return SIMPLE_MELEE;
            }

            @Override
            public List<RewardType> getChildren() {
                return children;
            }
        }

        public enum SIMPLE_RANGE_TYPES implements RewardType {
            LIGHTCROSSBOW("An average light crossbow."),
            DART("An average dart."),
            SHORTBOW("An average shortbow."),
            SLING("An average sling."),
            SPECIAL("A special simple ranged weapon.{qualifier}");

            final String description;
            final List<RewardType> children;
            SIMPLE_RANGE_TYPES(String description, RewardType... children) {
                this.description = description;
                this.children = Arrays.asList(children);
            }

            @Override
            public String getName() {
                return this.name();
            }

            @Override
            public String getDescription() {
                return description;
            }

            @Override
            public RewardType getParent() {
                return SIMPLE_RANGE;
            }

            @Override
            public List<RewardType> getChildren() {
                return children;
            }
        }

        public enum MARTIAL_MELEE_TYPES implements RewardType {
            BATTLEAXE("An average battleaxe."),
            FLAIL("An average flail."),
            GLAIVE("An average glaive."),
            GREATAXE("An average greataxe."),
            GREATSWORD("An average great-sword."),
            HALBERD("An average halberd."),
            LANCE("An average lance."),
            LONGSWORD("An average long-sword."),
            MAUL("An average maul."),
            MORNINGSTAR("An average morning-star."),
            PIKE("An average pike."),
            RAPIER("An average rapier."),
            SCIMITAR("An average scimitar."),
            SHORTSWORD("An average short-sword."),
            TRIDENT("An average trident."),
            WARPICK("An average war pick."),
            WARHAMMER("An average war-hammer."),
            WHIP("An average whip."),
            SPECIAL("A special martial melee weapon.{qualifier}");

            final String description;
            final List<RewardType> children;
            MARTIAL_MELEE_TYPES(String description, RewardType... children) {
                this.description = description;
                this.children = Arrays.asList(children);
            }

            @Override
            public String getName() {
                return this.name();
            }

            @Override
            public String getDescription() {
                return description;
            }

            @Override
            public RewardType getParent() {
                return MARTIAL_MELEE;
            }

            @Override
            public List<RewardType> getChildren() {
                return children;
            }
        }

        public enum MARTIAL_RANGE_TYPES implements RewardType {
            BLOWGUN("An average blowgun."),
            HANDCROSSBOW("An average hand crossbow."),
            HEAVYCROSSBOW("An average heavy crossbow."),
            LONGBOW("An average longbow."),
            NET("An average net."),
            SPECIAL("A special martial ranged weapon.{qualifier}");

            final String description;
            final List<RewardType> children;
            MARTIAL_RANGE_TYPES(String description, RewardType... children) {
                this.description = description;
                this.children = Arrays.asList(children);
            }

            @Override
            public String getName() {
                return this.name();
            }

            @Override
            public String getDescription() {
                return description;
            }

            @Override
            public RewardType getParent() {
                return MARTIAL_RANGE;
            }

            @Override
            public List<RewardType> getChildren() {
                return children;
            }
        }

    }

    public enum VALUABLE_TYPES implements RewardType {
        ART("A piece of valuable artwork.{qualifier}"),
        GEMS("A collection of valuable gems.{qualifier}"),
        COINS("A bag of coins.{qualifier}"),
        ARTIFACTS("An interesting artifact.{qualifier}"),
        SPECIAL("A strange item of value.{qualifier}");

        final String description;
        final List<RewardType> children;
        VALUABLE_TYPES(String description, RewardType... children) {
            this.description = description;
            this.children = Arrays.asList(children);
        }

        @Override
        public String getName() {
            return this.name();
        }

        @Override
        public String getDescription() {
            return description;
        }

        @Override
        public RewardType getParent() {
            return VALUABLE;
        }

        @Override
        public List<RewardType> getChildren() {
            return children;
        }
    }
}

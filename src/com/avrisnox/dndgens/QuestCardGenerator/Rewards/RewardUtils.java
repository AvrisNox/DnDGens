package com.avrisnox.dndgens.QuestCardGenerator.Rewards;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RewardUtils {
    static final Pattern p = Pattern.compile("(?:\\{)(.*?)(?:})");
    private RewardUtils() {}

    public static List<String> getRewardFormatNames(RewardType rewardType) {
        List<String> formatNames = new LinkedList<>();
        String description = rewardType.getDescription();
        Matcher m = p.matcher(description);
        while(m.find())
            formatNames.add(m.group(1));
        return formatNames;
    }

    public static String formatRewardTypeString(RewardType rewardType, HashMap<String, String> params) {
        String description = rewardType.getDescription();
        Matcher m = p.matcher(description);
        while(m.find()) {
            String key = m.group(1);
            String value = params.get(key);
            description = m.replaceFirst((value == null ? "" : " " + value));
            m = p.matcher(description);
        }
        return description;
    }

    public static float getRewardCost(Reward reward) {
        float cost = reward.getCost();
        List<RewardModifier> mods = reward.getMods();
        if(mods != null && mods.size() > 0)
            for(RewardModifier mod : mods)
                cost *= mod.costModifier;
        return cost;
    }

    public static List<RewardType> getRewardAndChildren(RewardType base) {
        List<RewardType> types = new ArrayList<>();
        types.add(base);
        for(int i = 0; i < types.size(); i++) {
            RewardType current = types.get(i);
            List<RewardType> children = current.getChildren();
            if (children != null)
                types.addAll(children);
        }
        return types;
    }
}

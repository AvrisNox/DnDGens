package com.avrisnox.dndgens.QuestCardGenerator.Rewards;

import java.util.List;

public interface RewardType {
    String getName();
    String getDescription();
    RewardType getParent();
    List<RewardType> getChildren();
}

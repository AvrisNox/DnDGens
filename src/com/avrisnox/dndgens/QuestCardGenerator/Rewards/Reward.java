package com.avrisnox.dndgens.QuestCardGenerator.Rewards;

import java.util.LinkedList;
import java.util.List;

public class Reward {
    float cost;
    float chance;
    RewardType type;
    List<RewardModifier> mods;

    public Reward() {
        this(
                0.0f,
                0.0f,
                null,
                null
        );
    }

    public Reward(float cost, float chance, RewardType type, List<RewardModifier> mods) {
        this.cost = cost;
        this.chance = chance;
        this.type = type;
        this.mods = mods;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public float getChance() {
        return chance;
    }

    public void setChance(float chance) {
        this.chance = chance;
    }

    public RewardType getType() {
        return type;
    }

    public void setType(RewardType type) {
        this.type = type;
    }

    public void addRewardModifier(RewardModifier mod) {
        if(mods == null)
            mods = new LinkedList<>();
        mods.add(mod);
    }

    public boolean remRewardModifier(RewardModifier mod) {
        if(mods == null)
            return false;
        return mods.remove(mod);
    }

    public void clearRewardModifiers() {
        mods = new LinkedList<>();
    }

    public List<RewardModifier> getMods() {
        return mods;
    }

    public void setMods(List<RewardModifier> mods) {
        this.mods = mods;
    }
}

package com.avrisnox.dndgens.QuestCardGenerator;

import com.avrisnox.dndgens.QuestCardGenerator.Objectives.ObjectiveType;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

@SuppressWarnings("unused")
public class Fixer {
    String name;
    String description;
    Image icon;
    HashMap<ObjectiveType, Float> objAffinity;

    public Fixer() {
        this(
                "An unknown fixer.",
                "This fixer is unknown to you.",
                (Image)null,
                null
        );
    }

    public Fixer(String name, String description, Image icon, HashMap<ObjectiveType, Float> objAffinity) {
        this.name = name;
        this.description = description;
        this.icon = icon;
        if(objAffinity == null) this.objAffinity = new HashMap<>();
        else this.objAffinity = objAffinity;
    }

    public Fixer(String name, String description, String iconPath, HashMap<ObjectiveType, Float> objAffinity) {
        this(
                name,
                description,
                (Image)null,
                objAffinity
        );
        setIcon(iconPath);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Image getIcon() {
        return icon;
    }

    public void setIcon(Image icon) {
        this.icon = icon;
    }

    public boolean setIcon(String path) {
        boolean result = false;
        try {
            icon = ImageIO.read(new File(path));
            result = true;
        } catch(IOException ignored) {
            System.out.println("Failed to set Fixer " + this.getName() + " icon with path " + path);
        }
        return result;
    }

    public HashMap<ObjectiveType, Float> getObjAffinity() {
        return objAffinity;
    }

    public void setObjAffinity(HashMap<ObjectiveType, Float> objAffinity) {
        this.objAffinity = objAffinity;
    }

    public void updateObjAffinity(ObjectiveType objType, float objAffinity) {
        this.objAffinity.put(objType, objAffinity);
    }

    public float getAffinityForObjectiveType(ObjectiveType objectiveType) {
        if(objAffinity == null || !objAffinity.containsKey(objectiveType))
            return 0.0f;
        return objAffinity.get(objectiveType);
    }
}

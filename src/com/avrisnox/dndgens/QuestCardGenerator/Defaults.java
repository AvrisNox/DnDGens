package com.avrisnox.dndgens.QuestCardGenerator;

import com.avrisnox.dndgens.QuestCardGenerator.Objectives.ObjectiveModifier;
import com.avrisnox.dndgens.QuestCardGenerator.Objectives.ObjectiveType;
import com.avrisnox.dndgens.QuestCardGenerator.Objectives.ObjectiveTypes;
import com.avrisnox.dndgens.QuestCardGenerator.Objectives.ObjectiveUtils;
import com.avrisnox.dndgens.QuestCardGenerator.Rewards.RewardModifier;
import com.avrisnox.dndgens.QuestCardGenerator.Rewards.RewardType;
import com.avrisnox.dndgens.QuestCardGenerator.Rewards.RewardTypes;
import com.avrisnox.dndgens.QuestCardGenerator.Rewards.RewardUtils;

import java.awt.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class Defaults {
    /* Default ObjectiveType lists */
    static List<ObjectiveType> exploreTypes = ObjectiveUtils.getObjectiveAndChildren(ObjectiveTypes.EXPLORE);
    static List<ObjectiveType> fightTypes = ObjectiveUtils.getObjectiveAndChildren(ObjectiveTypes.FIGHT);
    static List<ObjectiveType> scienceTypes = ObjectiveUtils.getObjectiveAndChildren(ObjectiveTypes.SCIENCE);
    static List<ObjectiveType> adventureTypes = ObjectiveUtils.getObjectiveAndChildren(ObjectiveTypes.ADVENTURE);
    static List<ObjectiveType> rogueTypes = ObjectiveUtils.getObjectiveAndChildren(ObjectiveTypes.ROGUE);
    static List<ObjectiveType> merchantTypes = ObjectiveUtils.getObjectiveAndChildren(ObjectiveTypes.MERCHANT);
    static List<ObjectiveType> guardTypes = ObjectiveUtils.getObjectiveAndChildren(ObjectiveTypes.GUARD);
    static List<ObjectiveType> destroyTypes = ObjectiveUtils.getObjectiveAndChildren(ObjectiveTypes.DESTROY);
    static List<ObjectiveType> defendInfTypes = ObjectiveUtils.getObjectiveAndChildren(ObjectiveTypes.DEFEND_INF);
    static List<ObjectiveType> craftTypes = ObjectiveUtils.getObjectiveAndChildren(ObjectiveTypes.CRAFT);
    static List<ObjectiveType> observeTypes = ObjectiveUtils.getObjectiveAndChildren(ObjectiveTypes.OBSERVE);
    static List<ObjectiveType> fenceTypes = ObjectiveUtils.getObjectiveAndChildren(ObjectiveTypes.FENCE);
    static List<ObjectiveType> surviveTypes = ObjectiveUtils.getObjectiveAndChildren(ObjectiveTypes.SURVIVE);
    static List<ObjectiveType> surpriseTypes = ObjectiveUtils.getObjectiveAndChildren(ObjectiveTypes.SURPRISE);

    /* Default RewardType lists */
    static List<RewardType> armorTypes = RewardUtils.getRewardAndChildren(RewardTypes.ARMOR);
    static List<RewardType> lightArmorTypes = RewardUtils.getRewardAndChildren(RewardTypes.ARMOR_TYPES.LIGHT);
    static List<RewardType> mediumArmorTypes = RewardUtils.getRewardAndChildren(RewardTypes.ARMOR_TYPES.MEDIUM);
    static List<RewardType> heavyArmorTypes = RewardUtils.getRewardAndChildren(RewardTypes.ARMOR_TYPES.HEAVY);
    static List<RewardType> weaponTypes = RewardUtils.getRewardAndChildren(RewardTypes.WEAPON);
    static List<RewardType> smWeaponTypes = RewardUtils.getRewardAndChildren(RewardTypes.WEAPON_TYPES.SIMPLE_MELEE);
    static List<RewardType> srWeaponTypes = RewardUtils.getRewardAndChildren(RewardTypes.WEAPON_TYPES.SIMPLE_RANGE);
    static List<RewardType> mmWeaponTypes = RewardUtils.getRewardAndChildren(RewardTypes.WEAPON_TYPES.MARTIAL_MELEE);
    static List<RewardType> mrWeaponTypes = RewardUtils.getRewardAndChildren(RewardTypes.WEAPON_TYPES.MARTIAL_RANGE);
    static List<RewardType> valuableTypes = RewardUtils.getRewardAndChildren(RewardTypes.VALUABLE);

    /* Default fixers */
    static List<Fixer> defaultFixers;
    static {
        defaultFixers = new LinkedList<>();

        HashMap<ObjectiveType, Float> independentAffinity = new HashMap<>();
        Fixer independent = new Fixer("Independent", "An independent contractor.", (Image)null, independentAffinity);


        HashMap<ObjectiveType, Float> explorerAffinity = new HashMap<>();
        for(ObjectiveType type : exploreTypes)
            explorerAffinity.put(type, 1.01f);
        String explorerImagePath = "explorer icon.png";
        Fixer explorer = new Fixer("Explorer's Guild", "The guild of legendary explorers.", Settings.IMAGE_BASEPATH + explorerImagePath, explorerAffinity);

        HashMap<ObjectiveType, Float> fighterAffinity = new HashMap<>();
        for(ObjectiveType type : fightTypes)
            fighterAffinity.put(type, 1.01f);
        String fighterImagePath = "fighter icon.png";
        Fixer fighter = new Fixer("Fighter's Guild", "A guild for the greatest fighters around.", Settings.IMAGE_BASEPATH + fighterImagePath, fighterAffinity);

        HashMap<ObjectiveType, Float> scientistAffinity = new HashMap<>();
        for(ObjectiveType type : scienceTypes)
            scientistAffinity.put(type, 1.01f);
        String scientistImagePath = "scientist icon.png";
        Fixer scientist = new Fixer("Scientist's Guild", "The greatest minds science has to offer.", Settings.IMAGE_BASEPATH + scientistImagePath, scientistAffinity);

        HashMap<ObjectiveType, Float> adventurerAffinity = new HashMap<>();
        for(ObjectiveType type : adventureTypes)
            adventurerAffinity.put(type, 1.01f);
        String adventurerImagePath = "adventurer icon.png";
        Fixer adventurer = new Fixer("Adventurer's Guild", "For the good of finding the new.", Settings.IMAGE_BASEPATH + adventurerImagePath, adventurerAffinity);

        HashMap<ObjectiveType, Float> rogueAffinity = new HashMap<>();
        for(ObjectiveType type : rogueTypes)
            rogueAffinity.put(type, 1.01f);
        String rogueImagePath = "rogue icon.png";
        Fixer rogue = new Fixer("Rogue's Guild", "Friends to the shadows.", Settings.IMAGE_BASEPATH + rogueImagePath, rogueAffinity);

        HashMap<ObjectiveType, Float> merchantAffinity = new HashMap<>();
        for(ObjectiveType type : merchantTypes)
            merchantAffinity.put(type, 1.01f);
        String merchantImagePath = "merchant icon.png";
        Fixer merchant = new Fixer("Merchant's Guild", "Economic growth is good for all.", Settings.IMAGE_BASEPATH + merchantImagePath, merchantAffinity);

        HashMap<ObjectiveType, Float> guardAffinity = new HashMap<>();
        for(ObjectiveType type : guardTypes)
            guardAffinity.put(type, 1.01f);
        String guardImagePath = "guard icon.png";
        Fixer guard = new Fixer("City Guard", "Safety. Security. For our people.", Settings.IMAGE_BASEPATH + guardImagePath, guardAffinity);
        
        HashMap<ObjectiveType, Float> goblinAffinity = new HashMap<>();
        for(ObjectiveType type : destroyTypes)
            goblinAffinity.put(type, 1.0f);
        for(ObjectiveType type : fightTypes)
            goblinAffinity.put(type, 1.0f);
        for(ObjectiveType type : merchantTypes)
            goblinAffinity.put(type, 1.0f);
        String goblinImagePath = "goblins icon.png";
        Fixer goblin = new Fixer("Quest Goblins", "For gold or death!", Settings.IMAGE_BASEPATH + goblinImagePath, goblinAffinity);
        
        HashMap<ObjectiveType, Float> daladeusAffinity = new HashMap<>();
        for(ObjectiveType type : destroyTypes)
            daladeusAffinity.put(type, 1.0f);
        for(ObjectiveType type : fightTypes)
            daladeusAffinity.put(type, 1.0f);
        for(ObjectiveType type : guardTypes)
            daladeusAffinity.put(type, 1.0f);
        String daladeusImagePath = "daladeus icon.png";
        Fixer daladeus = new Fixer("Daladeus' Fighters", "The fight for the people is a noble fight.", Settings.IMAGE_BASEPATH + daladeusImagePath, daladeusAffinity);

        HashMap<ObjectiveType, Float> mercenariesAffinity = new HashMap<>();
        for(ObjectiveType type : defendInfTypes)
            mercenariesAffinity.put(type, 1.0f);
        for(ObjectiveType type : fightTypes)
            mercenariesAffinity.put(type, 1.0f);
        for(ObjectiveType type : adventureTypes)
            mercenariesAffinity.put(type, 1.0f);
        String mercenariesImagePath = "mercenary icon.png";
        Fixer mercenaries = new Fixer("Mercenaries", "For payment on par with request.", Settings.IMAGE_BASEPATH + mercenariesImagePath, mercenariesAffinity);

        HashMap<ObjectiveType, Float> protectAffinity = new HashMap<>();
        for(ObjectiveType type : defendInfTypes)
            protectAffinity.put(type, 1.0f);
        for(ObjectiveType type : exploreTypes)
            protectAffinity.put(type, 1.0f);
        for(ObjectiveType type : guardTypes)
            protectAffinity.put(type, 1.0f);
        String protectImagePath = "protectorate icon.png";
        Fixer protect = new Fixer("The Protectorate", "Safety is our concern.", Settings.IMAGE_BASEPATH + protectImagePath, protectAffinity);
        
        HashMap<ObjectiveType, Float> galaxaAffinity = new HashMap<>();
        for(ObjectiveType type : craftTypes)
            galaxaAffinity.put(type, 1.0f);
        for(ObjectiveType type : exploreTypes)
            galaxaAffinity.put(type, 1.0f);
        for(ObjectiveType type : merchantTypes)
            galaxaAffinity.put(type, 1.0f);
        String galaxaImagePath = "galaxa icon.png";
        Fixer galaxa = new Fixer("Galaxa Fighters", "Reclamation of the peoples' treasures.", Settings.IMAGE_BASEPATH + galaxaImagePath, galaxaAffinity);

        HashMap<ObjectiveType, Float> wisemenAffinity = new HashMap<>();
        for(ObjectiveType type : craftTypes)
            wisemenAffinity.put(type, 1.0f);
        for(ObjectiveType type : scienceTypes)
            wisemenAffinity.put(type, 1.0f);
        for(ObjectiveType type : adventureTypes)
            wisemenAffinity.put(type, 1.0f);
        String wisemenImagePath = "wisemen icon.png";
        Fixer wisemen = new Fixer("The Wisemen", "Wisdom is an oft misused virtue.", Settings.IMAGE_BASEPATH + wisemenImagePath, wisemenAffinity);

        HashMap<ObjectiveType, Float> warriorsAffinity = new HashMap<>();
        for(ObjectiveType type : observeTypes)
            warriorsAffinity.put(type, 1.0f);
        for(ObjectiveType type : scienceTypes)
            warriorsAffinity.put(type, 1.0f);
        for(ObjectiveType type : guardTypes)
            warriorsAffinity.put(type, 1.0f);
        String warriorsImagePath = "warriors icon.png";
        Fixer warriors = new Fixer("Warriors of Progress", "The sacrifice of the few for the good of all.", Settings.IMAGE_BASEPATH + warriorsImagePath, warriorsAffinity);

        HashMap<ObjectiveType, Float> oraclesAffinity = new HashMap<>();
        for(ObjectiveType type : observeTypes)
            oraclesAffinity.put(type, 1.0f);
        for(ObjectiveType type : scienceTypes)
            oraclesAffinity.put(type, 1.0f);
        for(ObjectiveType type : merchantTypes)
            oraclesAffinity.put(type, 1.0f);
        String oraclesImagePath = "oracles icon.png";
        Fixer oracles = new Fixer("The Oracles", "Insight and foresight beat hindsight.", Settings.IMAGE_BASEPATH + oraclesImagePath, oraclesAffinity);
        
        HashMap<ObjectiveType, Float> thievesAffinity = new HashMap<>();
        for(ObjectiveType type : fenceTypes)
            thievesAffinity.put(type, 1.0f);
        for(ObjectiveType type : rogueTypes)
            thievesAffinity.put(type, 1.0f);
        String thievesImagePath = "thieves icon.png";
        Fixer thieves = new Fixer("Thieves' Guild", "", Settings.IMAGE_BASEPATH + thievesImagePath, thievesAffinity);

        HashMap<ObjectiveType, Float> heroesAffinity = new HashMap<>();
        for(ObjectiveType type : surviveTypes)
            heroesAffinity.put(type, 1.0f);
        for(ObjectiveType type : exploreTypes)
            heroesAffinity.put(type, 0.99f);
        for(ObjectiveType type : fightTypes)
            heroesAffinity.put(type, 0.99f);
        for(ObjectiveType type : scienceTypes)
            heroesAffinity.put(type, 0.99f);
        for(ObjectiveType type : adventureTypes)
            heroesAffinity.put(type, 0.99f);
        for(ObjectiveType type : merchantTypes)
            heroesAffinity.put(type, 0.99f);
        for(ObjectiveType type : guardTypes)
            heroesAffinity.put(type, 0.99f);
        String heroesImagePath = "heroes icon.png";
        Fixer heroes = new Fixer("Guild of Heroes", "", Settings.IMAGE_BASEPATH + heroesImagePath, heroesAffinity);

        defaultFixers.add(independent);
        defaultFixers.add(explorer);
        defaultFixers.add(fighter);
        defaultFixers.add(scientist);
        defaultFixers.add(adventurer);
        defaultFixers.add(rogue);
        defaultFixers.add(merchant);
        defaultFixers.add(guard);
        defaultFixers.add(goblin);
        defaultFixers.add(daladeus);
        defaultFixers.add(mercenaries);
        defaultFixers.add(protect);
        defaultFixers.add(galaxa);
        defaultFixers.add(wisemen);
        defaultFixers.add(warriors);
        defaultFixers.add(oracles);
        defaultFixers.add(thieves);
        defaultFixers.add(heroes);
    }

    /* Default objective modifiers */
    static List<ObjectiveModifier> defaultObjMods;
    static {
        defaultObjMods = new LinkedList<>();

        // Good Modifiers
        List<ObjectiveType> allyArchersObj = new LinkedList<>(exploreTypes);
        allyArchersObj.addAll(fightTypes);
        allyArchersObj.addAll(guardTypes);
        ObjectiveModifier allyArchers = new ObjectiveModifier(
                "Allied Archers",
                "Friendly archers from a nearby town will assist.",
                0.75f,
                Settings.IMAGE_BASEPATH + "ally archers.png",
                allyArchersObj
        );

        List<ObjectiveType> fairWeatherObj = new LinkedList<>(exploreTypes);
        fairWeatherObj.addAll(fightTypes);
        fairWeatherObj.addAll(adventureTypes);
        fairWeatherObj.addAll(guardTypes);
        fairWeatherObj.addAll(defendInfTypes);
        fairWeatherObj.addAll(observeTypes);
        fairWeatherObj.addAll(surviveTypes);
        ObjectiveModifier fairWeather = new ObjectiveModifier(
                "Fair Weather",
                "Fair weather makes it much easier to see.",
                0.95f,
                Settings.IMAGE_BASEPATH + "fair weather.png",
                fairWeatherObj
        );

        // Bad Modifiers
        List<ObjectiveType> moreEnemiesObj = new LinkedList<>(exploreTypes);
        moreEnemiesObj.addAll(fightTypes);
        moreEnemiesObj.addAll(guardTypes);
        ObjectiveModifier moreEnemies = new ObjectiveModifier(
                "More Enemies",
                "Something evil is causing more enemies to appear in encounters.",
                1.25f,
                Settings.IMAGE_BASEPATH + "more enemies.png",
                moreEnemiesObj
        );

        List<ObjectiveType> badWeatherObj = new LinkedList<>(exploreTypes);
        badWeatherObj.addAll(fightTypes);
        badWeatherObj.addAll(adventureTypes);
        badWeatherObj.addAll(guardTypes);
        badWeatherObj.addAll(defendInfTypes);
        badWeatherObj.addAll(observeTypes);
        badWeatherObj.addAll(surviveTypes);
        ObjectiveModifier badWeather = new ObjectiveModifier(
                "Bad Weather",
                "Bad weather makes it harder to see and concentrate.",
                1.05f,
                Settings.IMAGE_BASEPATH + "bad weather.png",
                badWeatherObj
        );

        // TODO: Gimme more objective types

        defaultObjMods.add(allyArchers);
        defaultObjMods.add(fairWeather);
        defaultObjMods.add(moreEnemies);
        defaultObjMods.add(badWeather);
    }

    /* Default reward modifiers */
    static List<RewardModifier> defaultRewardMods;
    static {
        defaultRewardMods = new LinkedList<>();

        // TODO: Gib some reward mods
    }

    private Defaults() {}

    static void verify() {
        System.out.println("Ready.");
    }
}

package com.avrisnox.dndgens.QuestCardGenerator;


import com.avrisnox.dndgens.QuestCardGenerator.Objectives.Objective;
import com.avrisnox.dndgens.QuestCardGenerator.Rewards.Reward;
import org.w3c.dom.Document;

import java.awt.*;
import java.util.*;
import java.util.List;

@SuppressWarnings("unused")
public class QuestCard {
	/* Front */
	String title;
	String description;
	String broker;
	float challenge;
	float party;
	float level;
	Fixer fixer;
	double time;

	/* Back */
	List<Objective> objectives;
	List<Reward> rewards;

	public QuestCard() {
		this(
				"A Simple Quest",
				"A simple quest given by an unknown person",
				"A Stranger",
				0.0f,
				1.0f,
				1.0f,
				null,
				null,
				null
		);
		addObjectives(new Objective());
		addRewards(new Reward());
	}

	public QuestCard(String title, String description, String broker, float challenge, float party, float level, Fixer fixer, List<Objective> objectives, List<Reward> rewards) {
		this.title = title;
		this.description = description;
		this.broker = broker;
		this.challenge = challenge;
		this.party = party;
		this.level = level;
		this.fixer = fixer;
		this.objectives = objectives;
		this.rewards = rewards;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Fixer getFixer() {
		return fixer;
	}

	public void setFixer(Fixer fixer) {
		this.fixer = fixer;
	}

	public String getBroker() {
		return broker;
	}

	public void setBroker(String broker) {
		this.broker = broker;
	}

	public float getChallenge() {
		return challenge;
	}

	public void setChallenge(float challenge) {
		this.challenge = challenge;
	}

	public float getParty() {
		return party;
	}

	public void setParty(float party) {
		this.party = party;
	}

	public float getLevel() {
		return level;
	}

	public void setLevel(float level) {
		this.level = level;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getTime() {
		return time;
	}

	public void setTime(double time) {
		this.time = time;
	}

	public void addObjectives(Objective objective) {
		if(objectives == null)
			objectives = new LinkedList<>();
		objectives.add(objective);
	}

	public boolean remObjectives(Objective objective) {
		if(objectives == null)
			return false;
		return objectives.remove(objective);
	}

	public void clearObjectives() {
		objectives = null;
	}

	public List<Objective> getObjectives() {
		return objectives;
	}

	public void setObjectives(List<Objective> objectives) {
		this.objectives = objectives;
	}

	public void addRewards(Reward reward) {
		if(rewards == null)
			rewards = new LinkedList<>();
		rewards.add(reward);
	}

	public boolean remRewards(Reward reward) {
		if(rewards == null)
			return false;
		return rewards.remove(reward);
	}

	public void clearRewards() {
		rewards = null;
	}

	public List<Reward> getRewards() {
		return rewards;
	}

	public void setRewards(List<Reward> rewards) {
		this.rewards = rewards;
	}

	public void drawCard(String path, Image background, Document layout) {
		// TODO: Create image at path, draw the background, then parse layout - needs DTD first
	}

	public static Fixer getSuggestedFixerFromObjectives(List<Objective> objectives, List<Fixer> fixers) {
		if (objectives == null || fixers == null)
			return null;

		HashMap<Fixer, Float> affinity = new HashMap<>();
		for(Fixer fixer : fixers)
			affinity.put(fixer, 0.0f);

		Queue<Objective> objQueue = new LinkedList<>(objectives);

		while(objQueue.peek() != null) {
			Objective current = objQueue.poll();
			if (!current.getChildren().isEmpty()) objQueue.addAll(current.getChildren());

			for(Fixer fixer : fixers) {
				float temp = affinity.get(fixer);
				temp += fixer.getAffinityForObjectiveType(current.getType());
				affinity.put(fixer, temp);
			}
		}

		float maxAffinity = 0.0f;
		Fixer maxFixer = null;
		for(Fixer fixer : affinity.keySet()) {
			if(maxFixer == null) {
				maxFixer = fixer;
				maxAffinity = affinity.get(fixer);
			} else {
				float newAffinity = affinity.get(fixer);
				if(newAffinity > maxAffinity) {
					maxFixer = fixer;
					maxAffinity = newAffinity;
				}
			}
		}

		return maxFixer;
	}

	public static float getChallengeFromPL(float party, float level) {
		return party * level / Settings.CHALLENGE_MODIFIER;
	}

	public static float getPartyFromCL(float challenge, float level) {
		return challenge * Settings.CHALLENGE_MODIFIER / level;
	}

	public static float getLevelFromCP(float challenge, float party) {
		return challenge * Settings.CHALLENGE_MODIFIER / party;
	}
}

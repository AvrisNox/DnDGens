package com.avrisnox.dndgens.QuestCardGenerator.Objectives;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("unused")
public class ObjectiveModifier {
    String name;
    String description;
    float challengeModifier;
    Image icon;
    List<ObjectiveType> possibleObjTypes;

    public ObjectiveModifier() {
        this(
                "Empty modifier",
                "An empty quest modifier",
                1.0f,
                (Image)null,
                null
        );
    }

    public ObjectiveModifier(String name, String description, float challengeModifier, Image icon, List<ObjectiveType> possibleObjTypes) {
        this.name = name;
        this.description = description;
        this.challengeModifier = challengeModifier;
        this.icon = icon;
        this.possibleObjTypes = possibleObjTypes;

    }

    public ObjectiveModifier(String name, String description, float challengeModifier, String iconPath, List<ObjectiveType> possibleObjTypes) {
        this(
                name,
                description,
                challengeModifier,
                (Image)null,
                possibleObjTypes
        );
        setIcon(iconPath);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getChallengeModifier() {
        return challengeModifier;
    }

    public void setChallengeModifier(float challengeModifier) {
        this.challengeModifier = challengeModifier;
    }

    public Image getIcon() {
        return icon;
    }

    public void setIcon(Image icon) {
        this.icon = icon;
    }

    public boolean setIcon(String path) {
        boolean result = false;
        try {
            icon = ImageIO.read(new File(path));
            result = true;
        } catch(IOException ignored) {
            System.out.println("Failed to set Objective Modifier " + this.getName() + " icon with path " + path);
        }
        return result;
    }

    public List<ObjectiveType> getPossibleObjTypes() {
        return possibleObjTypes;
    }

    public void setPossibleObjTypes(List<ObjectiveType> possibleObjTypes) {
        this.possibleObjTypes = possibleObjTypes;
    }

    public void addPossibleObjTypes(ObjectiveType possibleObjType) {
        if(possibleObjTypes == null)
            possibleObjTypes = new LinkedList<>();
        possibleObjTypes.add(possibleObjType);
    }

    public boolean remPossibleObjTypes(ObjectiveType possibleObjType) {
        if(possibleObjTypes == null)
            return false;
        return possibleObjTypes.remove(possibleObjType);
    }

    public void clearPossibleObjTypes() {
        possibleObjTypes = new LinkedList<>();
    }
}

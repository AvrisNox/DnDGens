package com.avrisnox.dndgens.QuestCardGenerator.Objectives;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressWarnings("unused")
public class ObjectiveUtils {
    static final Pattern p = Pattern.compile("(?:\\{)(.*?)(?:})");
    private ObjectiveUtils() {}

    public static List<String> getObjectiveFormatNames(ObjectiveType objType) {
        List<String> formatNames = new LinkedList<>();
        String description = objType.getDescription();
        Matcher m = p.matcher(description);
        while(m.find())
            formatNames.add(m.group(1));
        return formatNames;
    }

    public static String formatObjectiveTypeString(ObjectiveType objType, HashMap<String, String> params) {
        String description = objType.getDescription();
        Matcher m = p.matcher(description);
        while(m.find()) {
            String key = m.group(1);
            String value = params.get(key);
            description = m.replaceFirst((value == null ? "" : " " + value));
            m = p.matcher(description);
        }
        return description;
    }

    public static float getObjectiveDifficulty(Objective objective) {
        Queue<Objective> q = new LinkedList<>();
        q.add(objective);
        float difficulty = 0.0f;
        while(q.peek() != null) {
            Objective current = q.poll();
            List<Objective> children = current.getChildren();
            if(children != null && children.size() > 0)
                q.addAll(children);

            float currentDifficulty = current.getDifficulty();
            List<ObjectiveModifier> mods = current.getMods();
            if(mods != null && mods.size() > 0)
                for(ObjectiveModifier mod : mods)
                    currentDifficulty *= mod.challengeModifier;
            difficulty += currentDifficulty;
        }
        return difficulty;
    }

    public static List<ObjectiveType> getObjectiveAndChildren(ObjectiveType base) {
        List<ObjectiveType> types = new ArrayList<>();
        types.add(base);
        for(int i = 0; i < types.size(); i++) {
            ObjectiveType current = types.get(i);
            List<ObjectiveType> children = current.getChildren();
            if (children != null)
                types.addAll(children);
        }
        return types;
    }
}

package com.avrisnox.dndgens.QuestCardGenerator.Objectives;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("unused")
public class Objective {
	private boolean optional;
	private float difficulty;
	private ObjectiveType type;
	private List<Objective> children;
	private List<ObjectiveModifier> mods;
	private HashMap<String, String> params;

	public Objective() {
		this(
				true,
				0.0f,
				null,
				null,
				null,
				null
		);
	}

	public Objective(boolean isOptional, float difficulty, ObjectiveType objectiveType, List<Objective> children, List<ObjectiveModifier> mods, HashMap<String, String> params) {
		this.optional = isOptional;
		this.difficulty = difficulty;
		this.type = objectiveType;
		this.children = children;
		this.mods = mods;
		this.params = params;
	}

	public boolean isOptional() {
		return optional;
	}

	public void setOptional(boolean optional) {
		this.optional = optional;
	}

	public void addChild(Objective child) {
		if(children == null)
			children = new LinkedList<>();
		children.add(child);
	}

	public boolean remChild(Objective child) {
		if(children == null)
			return false;
		return children.remove(child);
	}

	public void clearChildren() {
		children = new LinkedList<>();
	}

	public List<Objective> getChildren() {
		return children;
	}

	public void setChildren(List<Objective> children) {
		this.children = children;
	}

	public ObjectiveType getType() {
		return type;
	}

	public void setType(ObjectiveType type) {
		this.type = type;
	}

	public void addParam(String name, String param) {
		if(params == null)
			params = new HashMap<>();
		params.put(name, param);
	}

	public boolean remParam(String name) {
		if(params == null)
			return false;
		return params.remove(name) != null;
	}

	public void clearParams() {
		params = new HashMap<>();
	}

	public HashMap<String, String> getParams() {
		return params;
	}

	public void setParams(HashMap<String, String> params) {
		this.params = params;
	}

	public float getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(float difficulty) {
		this.difficulty = difficulty;
	}

	public void addObjectiveModifier(ObjectiveModifier mod) {
		if(mods == null)
			mods = new LinkedList<>();
		mods.add(mod);
	}

	public boolean remObjectiveModifier(ObjectiveModifier mod) {
		if(mods == null)
			return false;
		return mods.remove(mod);
	}

	public void clearObjectiveModifiers() {
		mods = new LinkedList<>();
	}

	public List<ObjectiveModifier> getMods() {
		return mods;
	}

	public void setMods(List<ObjectiveModifier> mods) {
		this.mods = mods;
	}
}

package com.avrisnox.dndgens.QuestCardGenerator.Objectives;

import java.util.Arrays;
import java.util.List;

@SuppressWarnings("unused")
public enum ObjectiveTypes implements ObjectiveType {
    EXPLORE("Explore around{location}.", EXPLORE_TYPES.TRAVEL, EXPLORE_TYPES.INSPECT),
    FIGHT("Fight{enemy}.", FIGHT_TYPES.CLEAR_OUT, FIGHT_TYPES.KILL),
    SCIENCE("Perform science on{target}.", SCIENCE_TYPES.ASSESS, SCIENCE_TYPES.ANALYZE),
    ADVENTURE("Adventure to{location}.", ADVENTURE_TYPES.LOCATE, ADVENTURE_TYPES.EXTRACT),
    ROGUE("Do{action}.", ROGUE_TYPES.ASSASSINATE, ROGUE_TYPES.STEAL),
    MERCHANT("Barter with{target}.", MERCHANT_TYPES.ACQUIRE, MERCHANT_TYPES.SELL),
    GUARD("Guard{target}.", GUARD_TYPES.ESCORT, GUARD_TYPES.DEFEND),
    DESTROY("Destroy{target}.", DESTROY_TYPES.OBJECT, DESTROY_TYPES.FACTION, DESTROY_TYPES.LOCATION),
    DEFEND_INF("Defend{target} until you can no longer ensure its safety.", DEFEND_INF_TYPES.FOR, DEFEND_INF_TYPES.UNTIL, DEFEND_INF_TYPES.WHILE),
    CRAFT("Create{number}{item}.", CRAFT_TYPES.BREW, CRAFT_TYPES.ARMOR, CRAFT_TYPES.WEAPON, CRAFT_TYPES.ENCHANT, CRAFT_TYPES.TRINKET),
    OBSERVE("Observe{target}.", OBSERVE_TYPES.OBJECT_FOR, OBSERVE_TYPES.PERSON_FOR, OBSERVE_TYPES.LOCATION_FOR, OBSERVE_TYPES.OBJECT_UNTIL, OBSERVE_TYPES.PERSON_UNTIL, OBSERVE_TYPES.LOCATION_UNTIL),
    FENCE("Fence{number}{item}.", FENCE_TYPES.OBJECT),
    SURVIVE("Survive{target}.", SURVIVE_TYPES.FOR, SURVIVE_TYPES.UNTIL, SURVIVE_TYPES.WHILE),
    SURPRISE("It's a surprise!", SURPRISE_TYPES.OTHER);

    private final String description;
    private final List<ObjectiveType> children;
    ObjectiveTypes(String description, ObjectiveType... children) {
    	this.description = description;
    	this.children = Arrays.asList(children);
	}

	@Override
	public String getName() {
		return this.toString();
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public ObjectiveType getParent() {
		return null;
	}

	@Override
	public List<ObjectiveType> getChildren() {
		return children;
	}

	public enum EXPLORE_TYPES implements ObjectiveType {
		TRAVEL("Travel around{location}.", TRAVEL_TYPES.TO, TRAVEL_TYPES.IN, TRAVEL_TYPES.FROM, TRAVEL_TYPES.FROM_TO),
		INSPECT("Inspect{target}.", INSPECT_TYPES.OBJECT, INSPECT_TYPES.PERSON, INSPECT_TYPES.SCENE, INSPECT_TYPES.INTERROGATE);

		private final String description;
		private final List<ObjectiveType> children;
		EXPLORE_TYPES(String description, ObjectiveType... children) {
			this.description = description;
			this.children = Arrays.asList(children);
		}

		@Override
		public String getName() {
			return this.toString();
		}

		@Override
		public String getDescription() {
			return description;
		}

		@Override
		public ObjectiveType getParent() {
			return EXPLORE;
		}

		@Override
		public List<ObjectiveType> getChildren() {
			return children;
		}

		public enum TRAVEL_TYPES implements ObjectiveType {
			TO("Travel to{location}."),
			IN("Travel in{location}."),
			FROM("Travel from{location}."),
			FROM_TO("Travel from{location} to{location}.");

			private final String description;
			private final List<ObjectiveType> children;
			TRAVEL_TYPES(String description, ObjectiveType... children) {
				this.description = description;
				this.children = Arrays.asList(children);
			}

			@Override
			public String getName() {
				return this.toString();
			}

			@Override
			public String getDescription() {
				return description;
			}

			@Override
			public ObjectiveType getParent() {
				return TRAVEL;
			}

			@Override
			public List<ObjectiveType> getChildren() {
				return children;
			}
		}
		public enum INSPECT_TYPES implements ObjectiveType {
			OBJECT("Inspect{object}."),
			PERSON("Inspect{person}."),
			SCENE("Inspect{scene}."),
			INTERROGATE("Interrogate{suspect}.");

			private final String description;
			private final List<ObjectiveType> children;
			INSPECT_TYPES(String description, ObjectiveType... children) {
				this.description = description;
				this.children = Arrays.asList(children);
			}

			@Override
			public String getName() {
				return this.toString();
			}

			@Override
			public String getDescription() {
				return description;
			}

			@Override
			public ObjectiveType getParent() {
				return INSPECT;
			}

			@Override
			public List<ObjectiveType> getChildren() {
				return children;
			}
		}
	}

	public enum FIGHT_TYPES implements ObjectiveType {
		CLEAR_OUT("Clear out{location}.", CLEAR_OUT_TYPES.LOOT, CLEAR_OUT_TYPES.CAPTURE, CLEAR_OUT_TYPES.ELIMINATE, CLEAR_OUT_TYPES.CAPTURE_OR_ELIMINATE),
		KILL("Kill{count}{enemy}.", KILL_TYPES.HIDE, KILL_TYPES.TORTURE, KILL_TYPES.EXECUTE, KILL_TYPES.ELIMINATE, KILL_TYPES.BRUTALIZE);

		private final String description;
		private final List<ObjectiveType> children;
		FIGHT_TYPES(String description, ObjectiveType... children) {
			this.description = description;
			this.children = Arrays.asList(children);
		}

		@Override
		public String getName() {
			return this.toString();
		}

		@Override
		public String getDescription() {
			return description;
		}

		@Override
		public ObjectiveType getParent() {
			return FIGHT;
		}

		@Override
		public List<ObjectiveType> getChildren() {
			return children;
		}

		public enum CLEAR_OUT_TYPES implements ObjectiveType {
			ELIMINATE("Clear{location} of{enemy}."),
			LOOT("Loot{target}."),
			CAPTURE("Capture{enemy}."),
			CAPTURE_OR_ELIMINATE("Capture or eliminate{enemy}.");

			private final String description;
			private final List<ObjectiveType> children;
			CLEAR_OUT_TYPES(String description, ObjectiveType... children) {
				this.description = description;
				this.children = Arrays.asList(children);
			}

			@Override
			public String getName() {
				return this.toString();
			}

			@Override
			public String getDescription() {
				return description;
			}

			@Override
			public ObjectiveType getParent() {
				return CLEAR_OUT;
			}

			@Override
			public List<ObjectiveType> getChildren() {
				return children;
			}
		}
		public enum KILL_TYPES implements ObjectiveType {
			ELIMINATE("Eliminate{enemy}."),
			BRUTALIZE("Brutalize{enemy}."),
			HIDE("Hide{enemy} corpses."),
			EXECUTE("Execute{enemy}."),
			TORTURE("Torture{enemy}.");

			private final String description;
			private final List<ObjectiveType> children;
			KILL_TYPES(String description, ObjectiveType... children) {
				this.description = description;
				this.children = Arrays.asList(children);
			}

			@Override
			public String getName() {
				return this.toString();
			}

			@Override
			public String getDescription() {
				return description;
			}

			@Override
			public ObjectiveType getParent() {
				return KILL;
			}

			@Override
			public List<ObjectiveType> getChildren() {
				return children;
			}
		}
	}

	public enum SCIENCE_TYPES implements ObjectiveType {
		ANALYZE("Analyze{target}.", ANALYZE_TYPES.OBJECT, ANALYZE_TYPES.LOGIC, ANALYZE_TYPES.SCENE),
		ASSESS("Assess{target}.", ASSESS_TYPES.LOGIC, ASSESS_TYPES.LOCATION, ASSESS_TYPES.SITUATION);

		private final String description;
		private final List<ObjectiveType> children;
		SCIENCE_TYPES(String description, ObjectiveType... children) {
			this.description = description;
			this.children = Arrays.asList(children);
		}

		@Override
		public String getName() {
			return this.toString();
		}

		@Override
		public String getDescription() {
			return description;
		}

		@Override
		public ObjectiveType getParent() {
			return SCIENCE;
		}

		@Override
		public List<ObjectiveType> getChildren() {
			return children;
		}

		public enum ANALYZE_TYPES implements ObjectiveType {
			OBJECT("Analyze{object}."),
			SCENE("Analyze{scene}."),
			LOGIC("Analyze{logic}.");

			private final String description;
			private final List<ObjectiveType> children;
			ANALYZE_TYPES(String description, ObjectiveType... children) {
				this.description = description;
				this.children = Arrays.asList(children);
			}

			@Override
			public String getName() {
				return this.toString();
			}

			@Override
			public String getDescription() {
				return description;
			}

			@Override
			public ObjectiveType getParent() {
				return ANALYZE;
			}

			@Override
			public List<ObjectiveType> getChildren() {
				return children;
			}
		}
		public enum ASSESS_TYPES implements ObjectiveType {
			SITUATION("Assess{situation}."),
			LOCATION("Assess{location}."),
			LOGIC("Assess{logic}.");

			private final String description;
			private final List<ObjectiveType> children;
			ASSESS_TYPES(String description, ObjectiveType... children) {
				this.description = description;
				this.children = Arrays.asList(children);
			}

			@Override
			public String getName() {
				return this.toString();
			}

			@Override
			public String getDescription() {
				return description;
			}

			@Override
			public ObjectiveType getParent() {
				return ASSESS;
			}

			@Override
			public List<ObjectiveType> getChildren() {
				return children;
			}
		}
	}

	public enum ADVENTURE_TYPES implements ObjectiveType {
		LOCATE("Locate{target}.", LOCATE_TYPES.OBJECT, LOCATE_TYPES.PERSON, LOCATE_TYPES.LOCATION),
		EXTRACT("Extract{target}.", EXTRACT_TYPES.OBJECT, EXTRACT_TYPES.PERSON);

		private final String description;
		private final List<ObjectiveType> children;
		ADVENTURE_TYPES(String description, ObjectiveType... children) {
			this.description = description;
			this.children = Arrays.asList(children);
		}

		@Override
		public String getName() {
			return this.toString();
		}

		@Override
		public String getDescription() {
			return description;
		}

		@Override
		public ObjectiveType getParent() {
			return ADVENTURE;
		}

		@Override
		public List<ObjectiveType> getChildren() {
			return children;
		}

		public enum LOCATE_TYPES implements ObjectiveType {
			OBJECT("Locate{object}."),
			LOCATION("Find{location}."),
			PERSON("Locate{person}.");

			private final String description;
			private final List<ObjectiveType> children;
			LOCATE_TYPES(String description, ObjectiveType... children) {
				this.description = description;
				this.children = Arrays.asList(children);
			}

			@Override
			public String getName() {
				return this.toString();
			}

			@Override
			public String getDescription() {
				return description;
			}

			@Override
			public ObjectiveType getParent() {
				return LOCATE;
			}

			@Override
			public List<ObjectiveType> getChildren() {
				return children;
			}
		}
		public enum EXTRACT_TYPES implements ObjectiveType {
			OBJECT("Extract{object}."),
			PERSON("Extract{person}.");

			private final String description;
			private final List<ObjectiveType> children;
			EXTRACT_TYPES(String description, ObjectiveType... children) {
				this.description = description;
				this.children = Arrays.asList(children);
			}

			@Override
			public String getName() {
				return this.toString();
			}

			@Override
			public String getDescription() {
				return description;
			}

			@Override
			public ObjectiveType getParent() {
				return EXTRACT;
			}

			@Override
			public List<ObjectiveType> getChildren() {
				return children;
			}
		}
	}

	public enum ROGUE_TYPES implements ObjectiveType {
		ASSASSINATE("Assassinate{target}.", ASSASSINATE_TYPES.SECRETLY, ASSASSINATE_TYPES.SILENTLY, ASSASSINATE_TYPES.SILENT_AND_SECRET),
		STEAL("Steal{item}.", STEAL_TYPES.ANY_OBJECT, STEAL_TYPES.ALL_OBJECTS, STEAL_TYPES.MIN_OBJECTS, STEAL_TYPES.MAX_OBJECTS, STEAL_TYPES.KNOWN_OBJECT, STEAL_TYPES.UNKNOWN_OBJECT, STEAL_TYPES.BETWEEN_OBJECTS, STEAL_TYPES.MIN_OBJECTS_VALUE, STEAL_TYPES.MAX_OBJECTS_VALUE, STEAL_TYPES.BETWEEN_OBJECTS_VALUE);

		private final String description;
		private final List<ObjectiveType> children;
		ROGUE_TYPES(String description, ObjectiveType... children) {
			this.description = description;
			this.children = Arrays.asList(children);
		}

		@Override
		public String getName() {
			return this.toString();
		}

		@Override
		public String getDescription() {
			return description;
		}

		@Override
		public ObjectiveType getParent() {
			return ROGUE;
		}

		@Override
		public List<ObjectiveType> getChildren() {
			return children;
		}

		public enum ASSASSINATE_TYPES implements ObjectiveType {
			SILENTLY("Silently assassinate{target}."),
			SECRETLY("Secretly assassinate{target}."),
			SILENT_AND_SECRET("Silently and secretly assassinate{target}.");

			private final String description;
			private final List<ObjectiveType> children;
			ASSASSINATE_TYPES(String description, ObjectiveType... children) {
				this.description = description;
				this.children = Arrays.asList(children);
			}

			@Override
			public String getName() {
				return this.toString();
			}

			@Override
			public String getDescription() {
				return description;
			}

			@Override
			public ObjectiveType getParent() {
				return ASSASSINATE;
			}

			@Override
			public List<ObjectiveType> getChildren() {
				return children;
			}
		}
		public enum STEAL_TYPES implements ObjectiveType {
			KNOWN_OBJECT("Steal{object}."),
			UNKNOWN_OBJECT("Steal something specific."),
			ANY_OBJECT("Steal anything."),
			ALL_OBJECTS("Steal everything."),
			MIN_OBJECTS("Steal at least{number}{object}."),
			MIN_OBJECTS_VALUE("Steal at least{value}{currency} worth."),
			MAX_OBJECTS("Steal at most{number}{object}."),
			MAX_OBJECTS_VALUE("Steal at most{value}{currency} worth."),
			BETWEEN_OBJECTS("Steal between{minNumber} and{maxNumber} objects."),
			BETWEEN_OBJECTS_VALUE("Steal between{minValue} and{maxValue}{currency} worth.");

			private final String description;
			private final List<ObjectiveType> children;
			STEAL_TYPES(String description, ObjectiveType... children) {
				this.description = description;
				this.children = Arrays.asList(children);
			}

			@Override
			public String getName() {
				return this.toString();
			}

			@Override
			public String getDescription() {
				return description;
			}

			@Override
			public ObjectiveType getParent() {
				return STEAL;
			}

			@Override
			public List<ObjectiveType> getChildren() {
				return children;
			}
		}
	}

	public enum MERCHANT_TYPES implements ObjectiveType {
		ACQUIRE("Acquire{target}.", ACQUIRE_TYPES.INTEL, ACQUIRE_TYPES.OBJECT, ACQUIRE_TYPES.BLACK_MARKET),
		SELL("Sell{target}.", SELL_TYPES.OBJECT, SELL_TYPES.BARTER, SELL_TYPES.BLACK_MARKET);

		private final String description;
		private final List<ObjectiveType> children;
		MERCHANT_TYPES(String description, ObjectiveType... children) {
			this.description = description;
			this.children = Arrays.asList(children);
		}

		@Override
		public String getName() {
			return this.toString();
		}

		@Override
		public String getDescription() {
			return description;
		}

		@Override
		public ObjectiveType getParent() {
			return MERCHANT;
		}

		@Override
		public List<ObjectiveType> getChildren() {
			return children;
		}

		public enum ACQUIRE_TYPES implements ObjectiveType {
			OBJECT("Acquire{object}."),
			INTEL("Acquire intel on{target}."),
			BLACK_MARKET("Acquire{object} from the black market.");

			private final String description;
			private final List<ObjectiveType> children;
			ACQUIRE_TYPES(String description, ObjectiveType... children) {
				this.description = description;
				this.children = Arrays.asList(children);
			}

			@Override
			public String getName() {
				return this.toString();
			}

			@Override
			public String getDescription() {
				return description;
			}

			@Override
			public ObjectiveType getParent() {
				return ACQUIRE;
			}

			@Override
			public List<ObjectiveType> getChildren() {
				return children;
			}
		}
		public enum SELL_TYPES implements ObjectiveType {
			OBJECT("Sell{object}."),
			BARTER("Barter to sell{object}."),
			BLACK_MARKET("Barter{object} on the black market.");

			private final String description;
			private final List<ObjectiveType> children;
			SELL_TYPES(String description, ObjectiveType... children) {
				this.description = description;
				this.children = Arrays.asList(children);
			}

			@Override
			public String getName() {
				return this.toString();
			}

			@Override
			public String getDescription() {
				return description;
			}

			@Override
			public ObjectiveType getParent() {
				return SELL;
			}

			@Override
			public List<ObjectiveType> getChildren() {
				return children;
			}
		}
	}

	public enum GUARD_TYPES implements ObjectiveType {
		DEFEND("Defend{target}."),
		ESCORT("Escort{target}.");

		private final String description;
		private final List<ObjectiveType> children;
		GUARD_TYPES(String description, ObjectiveType... children) {
			this.description = description;
			this.children = Arrays.asList(children);
		}

		@Override
		public String getName() {
			return this.toString();
		}

		@Override
		public String getDescription() {
			return description;
		}

		@Override
		public ObjectiveType getParent() {
			return GUARD;
		}

		@Override
		public List<ObjectiveType> getChildren() {
			return children;
		}

		public enum DEFEND_TYPES implements ObjectiveType {
			UNTIL("Defend{target} until{occurrence}."),
			FOR("Defend{target} for{duration}{timeGranularity}."),
			WHILE("Defend{target} while{occurrence}.");

			private final String description;
			private final List<ObjectiveType> children;
			DEFEND_TYPES(String description, ObjectiveType... children) {
				this.description = description;
				this.children = Arrays.asList(children);
			}

			@Override
			public String getName() {
				return this.toString();
			}

			@Override
			public String getDescription() {
				return description;
			}

			@Override
			public ObjectiveType getParent() {
				return DEFEND;
			}

			@Override
			public List<ObjectiveType> getChildren() {
				return children;
			}
		}
		public enum ESCORT_TYPES implements ObjectiveType {
			TO("Escort{target} to{location}."),
			UNTIL("Escort{target} until{occurrence}.");

			private final String description;
			private final List<ObjectiveType> children;
			ESCORT_TYPES(String description, ObjectiveType... children) {
				this.description = description;
				this.children = Arrays.asList(children);
			}

			@Override
			public String getName() {
				return this.toString();
			}

			@Override
			public String getDescription() {
				return description;
			}

			@Override
			public ObjectiveType getParent() {
				return ESCORT;
			}

			@Override
			public List<ObjectiveType> getChildren() {
				return children;
			}
		}
	}

	public enum DESTROY_TYPES implements ObjectiveType {
		OBJECT("Destroy{object}."),
		LOCATION("Destroy{location}."),
		FACTION("Destroy{faction}.");

		private final String description;
		private final List<ObjectiveType> children;
		DESTROY_TYPES(String description, ObjectiveType... children) {
			this.description = description;
			this.children = Arrays.asList(children);
		}

		@Override
		public String getName() {
			return this.toString();
		}

		@Override
		public String getDescription() {
			return description;
		}

		@Override
		public ObjectiveType getParent() {
			return DESTROY;
		}

		@Override
		public List<ObjectiveType> getChildren() {
			return children;
		}
	}

	public enum DEFEND_INF_TYPES implements ObjectiveType {
		UNTIL("Defend{target} until{occurrence} or for as long as possible."),
		FOR("Defend{target} for{duration}{timeGranularity} or for as long as possible."),
		WHILE("Defend{target} while{occurrence} or for as long as possible.");

		private final String description;
		private final List<ObjectiveType> children;
		DEFEND_INF_TYPES(String description, ObjectiveType... children) {
			this.description = description;
			this.children = Arrays.asList(children);
		}

		@Override
		public String getName() {
			return this.toString();
		}

		@Override
		public String getDescription() {
			return description;
		}

		@Override
		public ObjectiveType getParent() {
			return DEFEND_INF;
		}

		@Override
		public List<ObjectiveType> getChildren() {
			return children;
		}
	}

	public enum CRAFT_TYPES implements ObjectiveType {
		WEAPON("Craft{number}{weapon}."),
		ARMOR("Craft{number}{armor}."),
		TRINKET("Craft{number}{trinket}."),
		ENCHANT("Enchant{number}{object}."),
		BREW("Brew{number}{drink}.");

		private final String description;
		private final List<ObjectiveType> children;
		CRAFT_TYPES(String description, ObjectiveType... children) {
			this.description = description;
			this.children = Arrays.asList(children);
		}

		@Override
		public String getName() {
			return this.toString();
		}

		@Override
		public String getDescription() {
			return description;
		}

		@Override
		public ObjectiveType getParent() {
			return CRAFT;
		}

		@Override
		public List<ObjectiveType> getChildren() {
			return children;
		}
	}

	public enum OBSERVE_TYPES implements ObjectiveType {
		OBJECT_FOR("Observe{object} for{occurrence_or_timeAndGranularity}."),
		OBJECT_UNTIL("Observe{object} until{occurrence_or_timeAndGranularity}."),
		LOCATION_FOR("Observe{location} for{occurrence_or_timeAndGranularity}."),
		LOCATION_UNTIL("Observe{location} until{occurrence_or_timeAndGranularity}."),
		PERSON_FOR("Observe{person} for{occurrence_or_timeAndGranularity}."),
		PERSON_UNTIL("Observe{person} until{occurrence_or_timeAndGranularity}.");

		private final String description;
		private final List<ObjectiveType> children;
		OBSERVE_TYPES(String description, ObjectiveType... children) {
			this.description = description;
			this.children = Arrays.asList(children);
		}

		@Override
		public String getName() {
			return this.toString();
		}

		@Override
		public String getDescription() {
			return description;
		}

		@Override
		public ObjectiveType getParent() {
			return OBSERVE;
		}

		@Override
		public List<ObjectiveType> getChildren() {
			return children;
		}
	}

	public enum FENCE_TYPES implements ObjectiveType {
		OBJECT("Fence{number}{object}.");

		private final String description;
		private final List<ObjectiveType> children;
		FENCE_TYPES(String description, ObjectiveType... children) {
			this.description = description;
			this.children = Arrays.asList(children);
		}

		@Override
		public String getName() {
			return this.toString();
		}

		@Override
		public String getDescription() {
			return description;
		}

		@Override
		public ObjectiveType getParent() {
			return FENCE;
		}

		@Override
		public List<ObjectiveType> getChildren() {
			return children;
		}
	}

	public enum SURVIVE_TYPES implements ObjectiveType {
		UNTIL("Survive until{occurrence}."),
		FOR("Survive for{duration}{timeGranularity}."),
		WHILE("Survive while{occurrence}.");

		private final String description;
		private final List<ObjectiveType> children;
		SURVIVE_TYPES(String description, ObjectiveType... children) {
			this.description = description;
			this.children = Arrays.asList(children);
		}

		@Override
		public String getName() {
			return this.toString();
		}

		@Override
		public String getDescription() {
			return description;
		}

		@Override
		public ObjectiveType getParent() {
			return SURVIVE;
		}

		@Override
		public List<ObjectiveType> getChildren() {
			return children;
		}
	}

	public enum SURPRISE_TYPES implements ObjectiveType {
		OTHER("It's a surprise!");

		private final String description;
		private final List<ObjectiveType> children;
		SURPRISE_TYPES(String description, ObjectiveType... children) {
			this.description = description;
			this.children = Arrays.asList(children);
		}

		@Override
		public String getName() {
			return this.toString();
		}

		@Override
		public String getDescription() {
			return description;
		}

		@Override
		public ObjectiveType getParent() {
			return SURPRISE;
		}

		@Override
		public List<ObjectiveType> getChildren() {
			return children;
		}
	}
}

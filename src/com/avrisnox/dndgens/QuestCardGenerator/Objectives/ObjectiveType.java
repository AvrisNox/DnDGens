package com.avrisnox.dndgens.QuestCardGenerator.Objectives;

import java.util.List;

@SuppressWarnings("unused")
public interface ObjectiveType {
    String getName();
    String getDescription();
    ObjectiveType getParent();
    List<ObjectiveType> getChildren();
}

package com.avrisnox.dndgens.QuestCardGenerator;

public class Settings {
    public static final float CHALLENGE_MODIFIER = 4.0f; // Lower numbers mean harder challenges
    public static final float TIME_MODIFIER = 1.0f; // Lower numbers mean shorter times
    public static final float OBJECTIVE_CHALLENGE_TOLERANCE = 0.1f; // Higher numbers might mean more random challenges
    public static final float REWARD_COST_TOLERANCE = 0.1f; // Higher numbers might mean more random rewards
    public static final float MULTI_OBJECTIVE_CHALLENGE_MOD = 1.1f; // >1 lowers the overall challenge of the quest card to account for multiple objectives in a single day
    public static final boolean DROP_EXCESS_OBJECTIVE_COST = false; // If true, objective costs that fall outside of tolerance but can't be included without going over tolerance will be dropped. If false, it will add an objective with minimum challenge.
    public static final boolean DROP_EXCESS_REWARD_COST = true; // If true, reward costs that fall outside of tolerance but can't be included without going over tolerance will be dropped. If false, it will add a reward with minimum cost.
    public static final String IMAGE_BASEPATH = "resources/";

    private Settings() {}
}
